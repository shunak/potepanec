module Potepan
  class CategoriesController < ApplicationController
    def show
      @taxon = Spree::Taxon.find(params[:id])
      @taxonomies = Spree::Taxonomy.includes(:root)
      @products = @taxon.all_products.includes(master: %i[default_price images])
    end
  end
end
