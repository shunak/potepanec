module Potepan
  class SampleController < ApplicationController
    def index; end

    def product_grid_left_sidebar; end

    def product_list_left_sidebar; end

    def single_product; end

    def cart_page; end

    def checkout_step1; end

    def checkout_step2; end

    def checkout_step3; end

    def checkout_complete; end

    def blog_left_sidebar; end

    def blog_right_sidebar; end

    def blog_single_left_sidebar; end

    def blog_single_right_sidebar; end

    def about_us; end

    def tokushoho; end

    def privacy_policy; end
  end
end
