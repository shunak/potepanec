require 'rails_helper'

RSpec.feature 'Categories_Features', type: :feature do
  describe 'link_to_categories' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, name: 'Mugs', taxonomy: taxonomy, parent: taxonomy.root) }
    let(:taxon_bags) { create(:taxon, name: 'Bags', taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:product) { create(:product, name: 'RUBY ON RAILS MUG', taxons: [taxon]) }
    let!(:another_product) { create(:product) }

    background do
      visit potepan_category_path(taxon.id)
    end

    scenario 'Mugsカテゴリ一覧ページにアクセス時、カテゴリ名、カテゴリ商品名などの情報が表示される' do
      within('ul.side-nav') do
        expect(page).to have_content(taxonomy.name)
        expect(page).to have_content(taxon.name)
        expect(page).to have_content(taxon.products.length)
      end
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(taxon.name)
      expect(page).to have_content(product.name)
      expect(page).to have_no_content(product.description)
    end

    scenario '別のカテゴリーに遷移できること' do
      visit potepan_category_path(taxon_bags.id)
      expect(current_path).to eq potepan_category_path(taxon_bags.id)
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(taxon.name)
      expect(page).to_not have_content(product.name)
      expect(page).to have_no_content(product.description)
    end

    scenario 'taxonに紐づかない商品が表示されない' do
      expect(page).to have_no_content(another_product.name)
    end

    scenario '商品名をクリック時、商品の詳細ページに移動する' do
      click_link product.name
      expect(page).to have_current_path potepan_product_path product.id
    end
  end
end
