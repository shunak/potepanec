require 'rails_helper'

RSpec.feature 'Products_Features', type: :feature do
  describe 'link_to_categories' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon], name: 'RUBY ON RAILS TOTE') }
    let!(:another_product) { create(:product) }

    background do
      visit potepan_product_path(product.id)
    end

    scenario 'カテゴリーページへ遷移できるか' do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
    end
  end
end
