require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title(page_title)" do
    it { expect(full_title("test")).to eq "test - BIGBAG Store" }
    it { expect(full_title("")).to eq "BIGBAG Store" }
  end
end