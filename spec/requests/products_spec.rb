require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :request do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    example 'showページへアクセスできること' do
      expect(response).to have_http_status(:success)
    end

    example '商品名が含まれていること' do
      expect(response.body).to include(product.name)
    end

    example '商品価格が含まれていること' do
      expect(response.body).to include(product.display_price.to_s)
    end

    example '商品説明が含まれていること' do
      expect(response.body).to include(product.description)
    end
  end
end
